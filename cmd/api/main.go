package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog/log"

	"project-template/configs"
	"project-template/routes"
	"project-template/wire"
)

func init() {
	configs.ProvideConfig("")
}

func main() {
	a, _ := wire.InitializeApplication()
	r := routes.InitializeRoute(a.Handlers)

	errCh := runServer(r)
	if err := <-errCh; err != nil {
		log.Fatal().Err(err).Msg("Error while running")
	}
}

func runServer(handler http.Handler) <-chan error {
	httpSrv := provideServer(handler, configs.AppCfg.Server.ExternalPort)

	errCh := make(chan error, 1)

	ctx, stop := signal.NotifyContext(
		context.Background(),
		os.Interrupt,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	go func() {
		<-ctx.Done()

		log.Info().Msg("Shutdown signal received")

		ctxTimeout, cancel := context.WithTimeout(context.Background(), 5*time.Second)

		defer func() {
			stop()
			cancel()
			close(errCh)
		}()

		httpSrv.SetKeepAlivesEnabled(false)

		if err := httpSrv.Shutdown(ctxTimeout); err != nil {
			errCh <- err
		}

		log.Info().Msg("Shutdown server completed")
	}()

	go func() {
		if err := httpSrv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			errCh <- err
			log.Fatal().Err(err).Msg("Server Listen and Serve error")
		}
	}()

	return errCh
}

func provideServer(handler http.Handler, addr string) *http.Server {
	httpSrv := &http.Server{
		Addr:    addr,
		Handler: handler,
	}

	return httpSrv
}
