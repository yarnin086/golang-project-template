start-container:
	docker-compose up -d --build

run:
	go run ./cmd/api/main.go

lint:
	golangci-lint run

build-windows:
	go build -o project-template.exe ./cmd/api/main.go

build-linux:
	go build -a -installsuffix cgo -o project-template ./cmd/api/main.go

test:
	go test ./... -cover
