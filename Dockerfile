FROM golang:1.18.2-alpine as builder

WORKDIR /build

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

COPY ./go.mod ./go.mod

RUN go mod download

COPY . .

RUN go build -a -installsuffix cgo -o project-template ./cmd/api/main.go


FROM alpine

RUN apk --no-cache add ca-certificates tzdata

WORKDIR /app

ENV GIN_MODE=release

COPY --from=builder /build/project-template .

COPY app.yaml ./app.yaml

EXPOSE 8080

CMD ["/app/project-template"]