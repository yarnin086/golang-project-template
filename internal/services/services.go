package services

import (
	"project-template/http/client"
	userService "project-template/internal/services/user"
)

type Services struct {
	UserService userService.UserService
	Clients     *client.Clients
}

func ProvideServices(userService userService.UserService, clients *client.Clients) *Services {
	return &Services{
		UserService: userService,
		Clients:     clients,
	}
}
