//go:generate mockgen -source=./user.go -destination=./mocks/mock_user_service.go -package=mocks

package user

import (
	"context"
	userRepository "project-template/internal/repositories/user"
)

type UserService interface {
	GetUserByID(ctx context.Context) error
}

type service struct {
	userRepository userRepository.UserRepository
}

func ProvideUserService(userRepository userRepository.UserRepository) UserService {
	return &service{
		userRepository: userRepository,
	}
}

func (svc *service) GetUserByID(ctx context.Context) error {
	_, err := svc.userRepository.ListAllUsers(ctx)
	if err != nil {
		return err
	}
	return nil
}
