//go:generate mockgen -source=./user.go -destination=./mocks/mock_user_handler.go -package=mocks

package user

import (
	"net/http"

	"github.com/gin-gonic/gin"

	userService "project-template/internal/services/user"
)

type UserHandler interface {
	ListAllUsers(c *gin.Context)
}

type Handler struct {
	UserService userService.UserService
}

func ProvideUserHandler(userService userService.UserService) UserHandler {
	return &Handler{
		UserService: userService,
	}
}

func (h *Handler) ListAllUsers(c *gin.Context) {
	err := h.UserService.GetUserByID(c)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"success": false, "error": err.Error()})
	}
	c.JSON(http.StatusOK, gin.H{"success": true, "error": ""})
}
