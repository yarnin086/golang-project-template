package handlers

import (
	userHandler "project-template/internal/handlers/user"
)

type Handlers struct {
	UserHandler userHandler.UserHandler
}

func ProvideHandlers(userHandler userHandler.UserHandler) *Handlers {
	return &Handlers{UserHandler: userHandler}
}
