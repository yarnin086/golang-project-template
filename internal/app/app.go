package app

import (
	"github.com/rs/zerolog/log"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	"project-template/observer"

	"project-template/http/client"
	"project-template/internal/handlers"
	"project-template/internal/repositories"
	"project-template/internal/services"
)

type ApplicationContainer struct {
	Handlers     *handlers.Handlers
	Services     *services.Services
	Repositories *repositories.Repositories
	Clients      *client.Clients
}

func initializeTracing() *tracesdk.TracerProvider {
	jExp, err := observer.NewTracerExporter()
	if err != nil {
		log.Fatal().Err(err).Msg("Error while providing new jaeger exporter")
	}

	tp := observer.NewTracerProvider()
	err = tp.ProvideTracerProvider(jExp)
	if err != nil {
		log.Fatal().Err(err).Msg("Error while providing tracer provider")
	}

	// Start tracer provider
	tp.StartTracing()

	return tp.TracerProvider
}
