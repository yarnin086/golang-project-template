package repositories

import (
	userRepository "project-template/internal/repositories/user"
)

type Repositories struct {
	UserRepository userRepository.UserRepository
}

func ProvideRepositories(userRepository userRepository.UserRepository) *Repositories {
	return &Repositories{
		UserRepository: userRepository,
	}
}
