//go:generate mockgen -source=./user.go -destination=./mocks/mock_user_repository.go -package=mocks

package user

import (
	"context"

	"gorm.io/gorm"

	"project-template/internal/entities"
)

type UserRepository interface {
	ListAllUsers(ctx context.Context) (entities.User, error)
}

type repository struct {
	db *gorm.DB
}

func ProvideUserRepository(db *gorm.DB) UserRepository {
	return &repository{db: db}
}

func (r *repository) ListAllUsers(ctx context.Context) (entities.User, error) {
	return entities.User{}, nil
}
