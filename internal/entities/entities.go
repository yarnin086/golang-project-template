package entities

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"project-template/configs"
)

type Entities struct {
	DB *gorm.DB
}

func ProvideGormDB() *gorm.DB {
	db, err := gorm.Open(mysql.Open(getDsn()), &gorm.Config{})
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialize database. unable to create connection pool")
	}

	conn, err := db.DB()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialize database. unable to get database")
	}

	err = conn.Ping()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialize database. unable to connect to database")
	}

	if configs.AppCfg.DB.AutoMigrate {
		log.Info().Msg("Auto migrating database...")

		err = db.AutoMigrate(&User{}, &Order{})
		if err != nil {
			log.Fatal().Err(err).Msg("Auto migrate database failed")
		}
	}

	log.Info().Msg("Auto migrate database successfully")

	return db
}

func ProvideEntities(db *gorm.DB) *Entities {
	return &Entities{DB: db}
}

func getDsn() string {
	dbCfg := configs.AppCfg.DB
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=true&tls=false", dbCfg.Username, dbCfg.Password, dbCfg.Host, dbCfg.Port, dbCfg.DBName)
}
