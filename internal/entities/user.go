package entities

import "gorm.io/gorm"

type User struct {
	ID        uint           `gorm:"primaryKey"`
	Name      string         `gorm:"type:varchar(50) not null;uniqueIndex:user_name_unq;index:user_name_idx"`
	Email     string         `gorm:"type:varchar(50) not null;uniqueIndex:user_email_unq;index:user_email_idx"`
	CreatedAt int64          `gorm:"autoCreateTime"`
	UpdatedAt int64          `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at"`
}
