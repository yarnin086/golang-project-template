package entities

import "gorm.io/gorm"

type Order struct {
	ID        uint
	UserID    uint
	Amount    float64
	CreatedAt int64          `gorm:"autoCreateTime"`
	UpdatedAt int64          `gorm:"autoUpdateTime"`
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at"`
}
