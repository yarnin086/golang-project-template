package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"project-template/graph/generated"
	"project-template/graph/model"
)

func (r *mutationResolver) UserMutations(ctx context.Context) (*model.UserMutations, error) {
	return &model.UserMutations{}, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
