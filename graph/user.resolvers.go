package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"project-template/graph/generated"
	"project-template/graph/model"
)

func (r *userMutationsResolver) CreateUser(ctx context.Context, obj *model.UserMutations, in model.UserInput) (*model.User, error) {
	return &model.User{ID: "UID001", Name: in.Name}, nil
}

func (r *userQueriesResolver) Users(ctx context.Context, obj *model.UserQueries, id int) ([]*model.User, error) {
	return []*model.User{{ID: "UID002", Name: "Jane Doe"}, {ID: "UID003", Name: "Joy Doe"}}, nil
}

// UserMutations returns generated.UserMutationsResolver implementation.
func (r *Resolver) UserMutations() generated.UserMutationsResolver { return &userMutationsResolver{r} }

// UserQueries returns generated.UserQueriesResolver implementation.
func (r *Resolver) UserQueries() generated.UserQueriesResolver { return &userQueriesResolver{r} }

type userMutationsResolver struct{ *Resolver }
type userQueriesResolver struct{ *Resolver }
