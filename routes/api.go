package routes

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"project-template/configs"

	"project-template/constant"
	"project-template/graph"
	"project-template/graph/generated"
	"project-template/internal/handlers"
	"project-template/middlewares"
)

func InitializeRoute(handlers *handlers.Handlers) *gin.Engine {
	r := gin.Default()

	if configs.AppCfg.Server.Debug {
		gin.SetMode(gin.DebugMode)
	}

	_ = middlewares.NewMiddleware()

	basePathRoute := r.Group(constant.APIBasePath)
	{
		usrRoutes := basePathRoute.Group("users")
		{
			usrRoutes.GET("", handlers.UserHandler.ListAllUsers)
		}
	}

	gqlRoute := r.Group("graphql")
	{
		gqlRoute.POST("/query", graphqlHandler())
		gqlRoute.GET("/playground", playgroundHandler())
	}

	return r
}

// Defining the Graphql handler
func graphqlHandler() gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	// Resolver is in the resolver.go file
	h := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/graphql/query")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
