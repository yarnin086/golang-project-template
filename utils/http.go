package utils

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"
)

func BuildURL(segments ...string) string {
	b := strings.Builder{}

	b.WriteString("")

	for _, s := range segments {
		b.WriteString("/")
		b.WriteString(url.PathEscape(s))
	}
	return b.String()
}

func BuildRequestBody(obj interface{}) (io.Reader, error) {
	if obj == nil {
		return nil, nil
	}

	switch v := obj.(type) {
	case string:
		return bytes.NewReader([]byte(v)), nil
	case []byte:
		return bytes.NewReader(v), nil
	default:
		b, err := json.Marshal(obj)
		if err != nil {
			return nil, err
		}
		return bytes.NewReader(b), nil
	}
}

func AddHeader(headers []map[string]string, req *http.Request) {
	for _, header := range headers {
		for k, v := range header {
			req.Header.Set(k, v)
		}
	}
}
