package configs

import (
	"fmt"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/rs/zerolog/log"
)

type App struct {
	Common   *Common   `yaml:"common"`
	Server   *Server   `yaml:"server"`
	DB       *DB       `yaml:"db"`
	Tracer   *Tracer   `yaml:"tracer"`
	Observer *Observer `yaml:"observer"`
	Monitor  *Monitor  `yaml:"monitor"`
}

var (
	AppCfg = &App{}
)

func ProvideConfig(path string) {
	err := cleanenv.ReadConfig(fmt.Sprintf("%sapp.yaml", path), AppCfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Application configuration initialize failed")
	}

	_ = cleanenv.ReadConfig("app-override.yaml", AppCfg)
}
