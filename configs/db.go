package configs

type DB struct {
	Host                  string `yaml:"host" env:"DB_HOST"`
	Port                  int    `yaml:"port" env:"DB_PORT"`
	Username              string `yaml:"username" env:"DB_USERNAME"`
	Password              string `yaml:"password" env:"DB_PASSWORD"`
	DBName                string `yaml:"db_name" env:"DB_NAME"`
	PoolSize              int    `yaml:"pool_size" env:"DB_POOL_SIZE"`
	AutoMigrate           bool   `yaml:"auto_migrate" env:"DB_AUTO_MIGRATE"`
	SkipErrRecordNotFound bool   `yaml:"skip_err_record_not_found" env:"DB_SKIP_ERR_RECORD_NOT_FOUND"`
}
