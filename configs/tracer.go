package configs

type Tracer struct {
	Jaeger *Jaeger `yaml:"jaeger"`
}

type Jaeger struct {
	Protocol string `yaml:"protocol" env:"JAEGER_PROTOCOL"`
	Domain   string `yaml:"domain" env:"JAEGER_DOMAIN"`
	Port     string `yaml:"port" env:"JAEGER_PORT"`
	Path     string `yaml:"path" env:"JAEGER_PATH"`
}
