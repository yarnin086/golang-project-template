package configs

type Server struct {
	ExternalPort string `yaml:"external_port" env:"SERVER_EXTERNAL_PORT"`
	Debug        bool   `yaml:"debug" env:"SERVER_DEBUG"`
}
