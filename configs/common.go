package configs

type Common struct {
	AppName     string `yaml:"app_name" env:"COMMON_APP_NAME"`
	RepoLink    string `yaml:"repo_link" env:"COMMON_REPO_LINK"`
	Description string `yaml:"description" env:"COMMON_DESCRIPTION"`
}
