package observer

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/rs/zerolog/log"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/semconv/v1.10.0"

	"project-template/configs"
)

type OpenTelemetry struct {
	TracerProvider *sdktrace.TracerProvider
}

func NewTracerProvider() *OpenTelemetry {
	return &OpenTelemetry{TracerProvider: &sdktrace.TracerProvider{}}
}

func NewTracerExporter() (*jaeger.Exporter, error) {
	je := fmt.Sprintf(
		"%s://%s:%s/%s",
		configs.AppCfg.Tracer.Jaeger.Protocol,
		configs.AppCfg.Tracer.Jaeger.Domain,
		configs.AppCfg.Tracer.Jaeger.Port,
		configs.AppCfg.Tracer.Jaeger.Path,
	)

	// Create the Jaeger exporter
	exp, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint(je)))
	if err != nil {
		return nil, err
	}

	return exp, nil
}

func (o *OpenTelemetry) ProvideTracerProvider(exporter *jaeger.Exporter) error {
	rand.Seed(time.Now().UnixNano())

	o.TracerProvider = sdktrace.NewTracerProvider(
		// Always be sure to batch in production.
		sdktrace.WithBatcher(exporter),
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		// Record information about this application in a Resource.
		sdktrace.WithResource(resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String("project_template"),
			attribute.String("environment", "dev"),
			attribute.Int64("ID", rand.Int63()),
		)),
	)
	return nil
}

func (o *OpenTelemetry) StartTracing() {
	// Register our TracerProvider as the global so any imported
	// instrumentation in the future will default to using it.
	otel.SetTracerProvider(o.TracerProvider)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Cleanly shutdown and flush telemetry when the application exits.
	defer func(ctx context.Context) {
		// Do not make the application hang when it is shutdown.
		ctx, cancel = context.WithTimeout(ctx, time.Second*5)
		defer cancel()
		if err := o.TracerProvider.Shutdown(ctx); err != nil {
			log.Fatal().Err(err).Msg("Error while running")
		}
	}(ctx)
}
