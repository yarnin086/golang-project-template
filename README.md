Project Template
===

[//]: # ()
[//]: # (Author: Yarnin.p)

[//]: # ()
[//]: # (## Prerequisites)

[//]: # (1. MariaDB Latest Version)

[//]: # (2. Go Version 1.18)

[//]: # (3. Docker & Docker Compose)

[//]: # (4. Makefile)

[//]: # ()
[//]: # (## Git repo link)

[//]: # ([gitlab.com/yarnin086/golang-project-template]&#40;https://gitlab.com/yarnin086/golang-project-template&#41;)

[//]: # ()
[//]: # (## Getting started)

[//]: # (1. Install golangci-lint)

[//]: # (```shell)

[//]: # (# binary will be $&#40;go env GOPATH&#41;/bin/golangci-lint)

[//]: # (curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $&#40;go env GOPATH&#41;/bin v1.45.2)

[//]: # ()
[//]: # (# see golangci-lint version)

[//]: # (golangci-lint --version)

[//]: # (```)

[//]: # ()
[//]: # ()
[//]: # (2. Start a container that used to run project)

[//]: # (```shell)

[//]: # (make start-container)

[//]: # ()
[//]: # (# If you use Windows and does not have Makefile)

[//]: # (# You can run docker compose command)

[//]: # (docker-compose up -d --build)

[//]: # (```)

[//]: # ()
[//]: # (3. Create app-override.yaml file by copy app.yaml &#40;env that used in project&#41;)

[//]: # ()
[//]: # ()
[//]: # (4. Run local)

[//]: # (```shell)

[//]: # (make run)

[//]: # ()
[//]: # (# If you use Windows and does not have Makefile)

[//]: # (# You can run go run command)

[//]: # (go run ./cmd/api/main.go)

[//]: # (```)

[//]: # ()
[//]: # ()
[//]: # (## Others)

[//]: # ()
[//]: # (- [golangci-lint installation link]&#40;https://golangci-lint.run/usage/install/&#41;)

[//]: # (- [MinGW download link &#40;which can install Makefile for Windows&#41;]&#40;https://sourceforge.net/projects/mingw/&#41;)