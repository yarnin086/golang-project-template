//go:generate mockgen -source=./external_client.go -destination=./mocks/mock_external_client.go -package=mocks

package externalclient

import (
	"context"
	"fmt"
	"net/http"

	"project-template/utils"
)

type ExternalClient interface {
	Authentication(ctx context.Context) error
}

type client struct {
	httpClient *http.Client
}

func ProvideExternalClient(httpClient *http.Client) ExternalClient {
	return &client{httpClient: httpClient}
}

func (c *client) Authentication(ctx context.Context) error {
	reader, err := utils.BuildRequestBody(nil)
	if err != nil {
		return err
	}

	url := utils.BuildURL("")

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, url, reader)
	if err != nil {
		return err
	}

	utils.AddHeader(nil, req)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}

	fmt.Printf("HTTP status code %d", resp.StatusCode)

	return nil
}
