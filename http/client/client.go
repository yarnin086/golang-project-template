package client

import (
	"net/http"

	"project-template/http/client/externalclient"
)

type Clients struct {
	ExternalClient externalclient.ExternalClient
}

func ProvideHTTPClient() *http.Client {
	return &http.Client{}
}

func ProvideClients(externalClient externalclient.ExternalClient) *Clients {
	return &Clients{
		ExternalClient: externalClient,
	}
}
