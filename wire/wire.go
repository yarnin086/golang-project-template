//go:build wireinject
// +build wireinject

//go:generate wire

package wire

import (
	"github.com/google/wire"

	"project-template/internal/app"
)

func InitializeApplication() (*app.ApplicationContainer, error) {
	wire.Build(MainBindingSet)
	return &app.ApplicationContainer{}, nil
}
