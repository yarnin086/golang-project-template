package wire

import (
	"github.com/google/wire"

	"project-template/http/client"
	"project-template/http/client/externalclient"
	"project-template/internal/app"
	"project-template/internal/entities"
	"project-template/internal/handlers"
	userHandler "project-template/internal/handlers/user"
	"project-template/internal/repositories"
	userRepository "project-template/internal/repositories/user"
	"project-template/internal/services"
	userService "project-template/internal/services/user"
)

var handlerSet = wire.NewSet(
	handlers.ProvideHandlers,
	userHandler.ProvideUserHandler,
)

var serviceSet = wire.NewSet(
	services.ProvideServices,
	userService.ProvideUserService,
)

var repositorySet = wire.NewSet(
	repositories.ProvideRepositories,
	userRepository.ProvideUserRepository,
)

var entitySet = wire.NewSet(
	entities.ProvideGormDB,
	entities.ProvideEntities,
)

var clientSet = wire.NewSet(
	client.ProvideHTTPClient,
	client.ProvideClients,
	externalclient.ProvideExternalClient,
)

var MainBindingSet = wire.NewSet(
	entitySet,
	handlerSet,
	serviceSet,
	repositorySet,
	clientSet,
	wire.Struct(new(app.ApplicationContainer), "*"),
)
